import init, { setup as setupWasm, Pizarra } from './pkg/pizarra_wasm.js';

function setupCanvas() {
  const canvas = document.getElementById('canvas');
  const width = canvas.scrollWidth;
  const height = canvas.scrollHeight;

  canvas.setAttribute('width', width);
  canvas.setAttribute('height', height);

  return { canvas, width, height };
}

function saveFileState(oldUrl, pizarra) {
  URL.revokeObjectURL(oldUrl);

  const exported = pizarra.to_svg();

  if (exported === undefined) {
    return;
  }

  const blob = new Blob([exported], {type: 'image/svg+xml'});
  const url = URL.createObjectURL(blob);

  document.getElementById('file-save').href = url;

  return url;
}

function setSelect(sel, val) {
  sel.value = val;
  sel.dispatchEvent(new Event('change'));
}

function main() {
  setupWasm();

  const { canvas, width, height } = setupCanvas();
  const context = canvas.getContext('2d');
  const pizarra = Pizarra.new(width, height);

  let buffer = null;
  let blobUrl = null;

  // Events
  canvas.addEventListener('mousedown', (event) => {
    pizarra.handle_mouse_button_pressed(event.button, event.offsetX, event.offsetY);
  });

  canvas.addEventListener('mouseup', (event) => {
    pizarra.handle_mouse_button_released(event.button, event.offsetX, event.offsetY);
  });

  canvas.addEventListener('mousemove', (event) => {
    pizarra.handle_mouse_move(event.offsetX, event.offsetY);
  });

  window.onresize = () => {
    const { width, height } = setupCanvas();
    pizarra.resize(width, height);
  };

  const color_btn = document.getElementById('color')
  color_btn.addEventListener('change', (event) => {
    pizarra.set_color(event.target.value);
  });

  const tool_select = document.getElementById('tool');
  tool_select.addEventListener('change', (event) => {
    pizarra.set_tool(event.target.value);
  });

  const zoom_in_btn = document.getElementById('zoom-in-btn')
  zoom_in_btn.addEventListener('click', () => {
    pizarra.zoom_in();
  });

  const zoom_out_btn = document.getElementById('zoom-out-btn');
  zoom_out_btn.addEventListener('click', () => {
    pizarra.zoom_out();
  });

  const home_btn = document.getElementById('zoom-home-btn');
  home_btn.addEventListener('click', () => {
    pizarra.home();
  });

  const edit_undo_btn = document.getElementById('edit-undo');
  edit_undo_btn.addEventListener('click', () => {
    pizarra.undo();
  });

  const edit_redo_btn = document.getElementById('edit-redo');
  edit_redo_btn.addEventListener('click', () => {
    pizarra.redo();
  });

  const thickness_range = document.getElementById('thickness-range');
  thickness_range.addEventListener('change', (e) => {
    pizarra.set_thickness(e.target.value);
    document.getElementById('thickness-dropdown').innerText = `Grosor (${e.target.value})`;
  });

  const alpha_range = document.getElementById('alpha-range');
  alpha_range.addEventListener('change', (e) => {
    pizarra.set_alpha(e.target.value);
    document.getElementById('alpha-dropdown').innerText = `Alpha (${(e.target.value * 100).toFixed(0)}%)`;
  });

  window.addEventListener('keyup', (event) => {
    if (event.key === '+') {
      zoom_in_btn.click();
    } else if (event.key === '-') {
      zoom_out_btn.click();
    } else if (event.key === '0') {
      home_btn.click();
    } else if (event.key === 'l') {
      setSelect(tool_select, 'path');
    } else if (event.key === 'r') {
      setSelect(tool_select, 'rectangle');
    } else if (event.key === 'p') {
      setSelect(tool_select, 'polygon');
    } else if (event.key === 'c') {
      setSelect(tool_select, 'circle');
    } else if (event.key === 'e') {
      setSelect(tool_select, 'ellipse');
    } else if (event.key === 'g') {
      setSelect(tool_select, 'eraser');
    }
  });

  const intro_modal = new bootstrap.Modal(document.getElementById('intro-modal'));
  intro_modal.show();

  // Rendering logic
  pizarra.render_base(context);
  pizarra.render_persistent(context);

  function renderLoop() {
    const mustRedraw = pizarra.pop_redraw();

    if (mustRedraw.all()) {
      pizarra.render_base(context);
      pizarra.render_persistent(context);

      buffer = new Promise((resolve) => {
        canvas.toBlob((blob) => {
          createImageBitmap(blob).then((buf) => {
            resolve(buf);
          });
        });
      });

      blobUrl = saveFileState(blobUrl, pizarra);

      pizarra.render_last_shape(context);
    } else if (mustRedraw.shape()) {
      if (buffer !== null) {
        buffer.then((buf) => {
          context.drawImage(buf, 0, 0);
          pizarra.render_last_shape(context);
        });
      } else {
        pizarra.render_last_shape(context);
      }
    }

    requestAnimationFrame(renderLoop);
  }

  requestAnimationFrame(renderLoop);
}

Promise.all([
  init(),
  new Promise((resolve) => {
    document.addEventListener('DOMContentLoaded', resolve);
  }),
]).then(main);
