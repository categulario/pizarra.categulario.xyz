use std::mem;

use wasm_bindgen::prelude::*;

use pizarra::app::ShouldRedraw;

pub struct Queue {
    redraw: ShouldRedraw,
}

impl Queue {
    pub fn new() -> Queue {
        Queue {
            redraw: ShouldRedraw::No,
        }
    }

    pub fn push(&mut self, redraw: ShouldRedraw) {
        if let ShouldRedraw::No = redraw {
            return;
        }

        match self.redraw {
            ShouldRedraw::All => {}
            ShouldRedraw::Shape => {
                if let ShouldRedraw::All = redraw {
                    self.redraw = ShouldRedraw::All;
                }
            }
            ShouldRedraw::No => {
                self.redraw = redraw;
            }
        }
    }

    pub fn pop(&mut self) -> Redraw {
        let redraw = ShouldRedraw::No;

        mem::replace(&mut self.redraw, redraw).into()
    }
}

#[wasm_bindgen]
#[derive(PartialEq)]
pub struct Redraw(ShouldRedraw);

#[wasm_bindgen]
impl Redraw {
    pub fn all(&self) -> bool {
        if let ShouldRedraw::All = self.0 {
            true
        } else {
            false
        }
    }

    pub fn shape(&self) -> bool {
        if let ShouldRedraw::Shape = self.0 {
            true
        } else {
            false
        }
    }
}

impl From<ShouldRedraw> for Redraw {
    fn from(r: ShouldRedraw) -> Redraw {
        Redraw(r)
    }
}
